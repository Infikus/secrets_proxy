#define _GNU_SOURCE

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#define LISTEN_LIMIT 1000
#define BUFFER_SIZE 8096

struct handle_client_arg {
  int client;
  struct sockaddr_in addr;
};

void* handle_client(struct handle_client_arg* arg);
int read_host_from_data(const char* data, char* host[], int* size);
int send_receive_request(const char* data, int data_size, const char* host, int origin_client);
int get_content_length(const char* data);
int create_server(const char* host, int port);
int start_server(int server);

int addr_to_ip(char* addr, char** name) {
  struct hostent* h = gethostbyname(addr);

  if (h == NULL) {
    *name = (char*) malloc(strlen(addr));
    strcpy(*name, addr);
    return -1;
  }

  struct in_addr **addr_list;

  addr_list = (struct in_addr**) h->h_addr_list;

  char* ip = inet_ntoa(*addr_list[0]);
  *name = (char*) calloc(strlen(ip) + 1, 1);

  strcpy(*name, inet_ntoa(*addr_list[0]));

  return 0;
}

int main(int argc, char* argv[]) {
  // Read arguments
  if (argc != 3) {
    fprintf(stderr, "usage: socket host port");
    exit(EXIT_FAILURE);
  }

  int server, port;
  char* addr_str = NULL;

  addr_to_ip(argv[1], &addr_str);
  port = strtol(argv[2], NULL, 10);

  // Create the server socket
  server = create_server(addr_str, port);

  start_server(server);

  free(addr_str);

  close(server);
  return 0;
}

int create_server(const char* addr_str, int port) {
  int server_fd;
  int opt = 1;
  struct sockaddr_in addr;

  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = inet_addr(addr_str);
  addr.sin_port = htons(port);

  if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    perror("socket failed");
    exit(EXIT_FAILURE);
  }

  if (setsockopt(server_fd, SOL_SOCKET,
        (SO_REUSEPORT | SO_REUSEADDR),
        &opt, sizeof(opt)) < 0) {
    perror("setsockopt failed");
    exit(EXIT_FAILURE);
  }

  if(bind(server_fd, (struct sockaddr *)(&addr), sizeof(addr)) < 0) {
    perror("bind failed");
    exit(EXIT_FAILURE);
  }

  return server_fd;
}

int start_server(int server) {
  struct sockaddr_in client_addr = { 0 };
  int client, client_addr_len;

  if (listen(server, LISTEN_LIMIT) < 0) {
    perror("listen failed");
    exit(EXIT_FAILURE);
  }

  while(1) {
    printf("Waiting for a client....\n");
    client = accept4(
      server,
      (struct sockaddr*)(&client_addr),
      (socklen_t*)&client_addr_len,
      SOCK_CLOEXEC
    );
    if (client < 0) {
      perror("accept failed");
      exit(EXIT_FAILURE);
    }
    pthread_t thread;

    struct handle_client_arg* handle_client_params = malloc(sizeof(struct handle_client_arg));
    handle_client_params->client = client;
    handle_client_params->addr = client_addr;

    pthread_create(&thread, NULL, (void*)handle_client, (void*)handle_client_params);
  }

  return 0;
}

void* handle_client(struct handle_client_arg* arg) {
  int client = arg->client;
  struct sockaddr_in addr = arg->addr;
  // number of bytes read
  char buffer[BUFFER_SIZE] = { 0 };
  char *host = NULL, *host_name = NULL;

  const char* ip = inet_ntoa(addr.sin_addr);

  printf("client %s connected\n", ip);

  int nbytes = read(client, buffer, BUFFER_SIZE - 1);

  if (nbytes < 0) {
    perror("read failed");
    exit(EXIT_FAILURE);
  } else if (nbytes == 0) {
    close(client);
    return 0;
  }

  printf("%s has received %d bytes\n", ip, nbytes);
  
  // Read host value to get the destination address
  int size = 0;
  read_host_from_data(buffer, &host, &size);
  addr_to_ip(host, &host_name);

  send_receive_request(buffer, nbytes, host_name, client);

  return 0;
}

int send_receive_request(const char* data, int data_size, const char* host, int origin_client) {
  int client = 0, status = 0;
  char buffer[BUFFER_SIZE];
  struct sockaddr_in addr;

  client = socket(AF_INET, SOCK_STREAM, 0);
  if (client < 0) {
    perror("socket failed");
    exit(EXIT_FAILURE);
  }

  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = inet_addr(host);
  addr.sin_port = htons(80);

  if (connect(client,(struct sockaddr *) &addr, sizeof(addr)) != 0) {
    perror("connect failed");
    exit(EXIT_FAILURE);
  }

  int content_length = 0;
  int bytes_read = 0;

  status = send(client, data, data_size, 0);
  if (status < 0) {
    perror("send failed");
    exit(EXIT_FAILURE);
  }

  while (1) {
    int size_received = recv(client, buffer, BUFFER_SIZE - 1, 0);

    if (size_received < 0) {
      perror("recv failed");
      exit(EXIT_FAILURE);
    }

    if (size_received == 0){
      break;
    }

    send(origin_client, buffer, size_received, 0);

    bytes_read += size_received;

    if (content_length == 0) {
      content_length = get_content_length(buffer);
    }
    if (content_length >= 0 && bytes_read >= content_length) {
      break;
    }
  }

  close(client);
  close(origin_client);

  return 0;
}



int read_host_from_data(const char* data, char* host[], int* size){
  char* host_regex = "[Hh]ost: ([^ \n\t\r:]*)";
  regex_t r;
  regmatch_t pmatch[2];

  int comp_status = regcomp(&r, host_regex, REG_EXTENDED|REG_NEWLINE);
  if (comp_status != 0){
    return comp_status;
  }

  int match_status = regexec(&r, data, 2, pmatch, 0);

  regfree(&r);
  // No match
  if (match_status != 0) {
    return -1;
  }

  int start = pmatch[1].rm_so;
  int end = pmatch[1].rm_eo;
  *size = end - start;

  *host = (char*) calloc(*size + 1, 1);
  strncpy (*host, &data[start], *size);

  return 0;
}

int get_content_length(const char* data) {
 const char* regex = "[cC]ontent-[Ll]ength ?: ?([0-9]+)";
 regex_t r;
 regmatch_t pmatch[2];

  if (regcomp(&r, regex, REG_EXTENDED|REG_NEWLINE) != 0) {
    return -1;
  }

  int match_status = regexec(&r, data, 2, pmatch, 0);

  regfree(&r);

  if (match_status != 0) {
    return -1;
  }

  int start = pmatch[1].rm_so;
  int end = pmatch[1].rm_eo;
  int size = end - start;

  char* length = (char*) calloc(size + 1, 1);
  strncpy (length, &(data[start]), size);

  int res = strtol(length, NULL, 10);
  free(length);

  return res;
}
